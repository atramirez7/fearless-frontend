import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);

    const [location, setLocation] = useState([]);
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [name,setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [startDate, setStartDate] = useState("");
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const [endDate, setEndDate] = useState("");
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const[description, setDescription] = useState("");
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const[maxAttendee, setMaxAttendee] = useState("");
    const handleMaxAttendeeChange = (event) => {
        const value = event.target.value;
        setMaxAttendee(value);
    }

    const[maxPresentation, setMaxPresentation] = useState("");
    const handleMaxPresentationChange = (event) => {
        const value = event.target.value;
        setMaxPresentation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const formdata = {};

        formdata.location = location;
        formdata.name = name;
        formdata.description = description;
        formdata.max_attendees = maxAttendee;
        formdata.max_presentations = maxPresentation;
        formdata.starts = startDate;
        formdata.ends = endDate;


        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formdata),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setMaxAttendee('');
          setStartDate('');
          setEndDate('');
          setDescription('');
          setMaxPresentation('');
          setLocation('');

        }
      }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data);
                setLocations(data.locations)

            }
        }
        useEffect(() => {
            fetchData();
          }, []);
        return ( <>
        <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <div className="mb-2">
                            <input onChange={handleNameChange} value={name} type="text" className="form-control" placeholder="Name" id="name" name="name" required />
                        </div>
                        <div className="mb-2">
                        <   div id="startshelp" className="form-text">Start Date</div>
                            <input onChange={handleStartDateChange} value={startDate} type="date" className="form-control" placeholder="Start Date" id="starts" name="starts" required />
                        </div>
                        <div className="mb-2">
                            <div id="endshelp" className="form-text">End Date</div>
                            <input onChange={handleEndDateChange} value={endDate} type="date" className="form-control" placeholder="End Date" id="ends" name="ends" required />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" rows="3" name="description"></textarea>
                        </div>
                        <div className="mb-2">
                            <input onChange={handleMaxPresentationChange} value={maxPresentation} type="text" className="form-control" placeholder="Maximum presentations" id="max_presentations" name="max_presentations" required />
                        </div>
                        <div className="mb-2">
                            <input onChange={handleMaxAttendeeChange} value={maxAttendee} type="text" className="form-control" placeholder="Maximum attendees" id="max_attendees" name="max_attendees" required />
                        </div>
                        <div className="mb-2">
                            <select onChange={handleLocationChange} className="form-select" id="location" name="location">
                                <option value="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option value={location.id} key={location.href}>
                                                {location.name}
                                            </option>
                                        )
                                    })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
        </>
        );
    }

export default ConferenceForm;
