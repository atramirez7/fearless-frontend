import React, { useEffect, useState } from 'react';

function LocationForm () {
    const [states, setStates] = useState([]);

    const [state, setState] = useState([]);
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const [name,setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [city,setCity] = useState("");
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const [roomCount,setRoomCount] = useState("");
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const formdata = {};

        formdata.room_count = roomCount;
        formdata.name = name;
        formdata.city = city;
        formdata.state = state;


        const locationURL = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formdata),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationURL, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setRoomCount('');
          setCity('');
          setState('');
        }
      }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data);
                setStates(data.states)

            }
        }
        useEffect(() => {
            fetchData();
          }, []);
        return ( <>
        <form onSubmit={handleSubmit} id="create-location-form">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        <div className="mb-2">
                            <input onChange={handleNameChange} type="text" className="form-control" placeholder="Name" id="name" name="name" value={name} required />
                        </div>
                        <div className="mb-2">
                            <input onChange={handleRoomCountChange} type="text" className="form-control" placeholder="Room count" id="room_count" name="room_count" value={roomCount} required />
                        </div>
                        <div className="mb-2">
                            <input onChange={handleCityChange} type="text" className="form-control" placeholder="City" id="city" name="city" value={city} required />
                        </div>
                        <div className="mb-2">
                            <select onChange={handleStateChange} className="form-select" id="state" name="state" required>
                            <option value="">State</option>
                            {states.map(state => {
                                return (
                                    <option value={Object.values(state)[0]} key={Object.values(state)[0]}>
                                        {Object.keys(state)[0]}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
        </>
        );
    }

export default LocationForm;
